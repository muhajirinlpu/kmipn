<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Entities\Accounts\User::query()->create([
            'type' => 'admin',
            'username' => 'admin',
            'password' => 'kmipn2k18',
            'name' => 'Administrator'
        ]);

        \App\Entities\Accounts\User::query()->create([
            'type' => 'competitor',
            'username' => 'debug-test',
            'password' => 'password',
            'name' => 'Debug Test'
        ]);
    }
}
